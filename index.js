// 3. Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.

fetch('https://jsonplaceholder.typicode.com/todos')
		.then((response) => response.json())
		.then((data) => console.log(data))

// 4. Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.


fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((data) => {
	console.log(data.map((printResult) =>	
	{
		return {
			'title':printResult.title
				}
	}))
})


// 5. Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((data) => console.log(data))



// 6. Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.



fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		completed: false,
		title: "title",
	})
})
.then((response) => response.json())
.then((data) => console.log(data))









// 7. Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.

fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		completed: false,
		id: 1,
		title: "create a to do list item",
		userId: 1
	})
})
.then((response) => response.json())
.then((data) => console.log(data))

// 8. Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API

fetch('https://jsonplaceholder.typicode.com/todos/2', {
	method: 'PUT',
	headers: {'Content-Type': 'application/json'},
	body: JSON.stringify({
		title: 'Updated post'
	})
})
.then((response) => response.json())
.then((data) => console.log(data))

/*
9. Update a to do list item by changing the data structure to contain 
the following properties:
a. Title
b. Description
c. Status
d. Date Completed
e. User ID
*/


function dateNow(){
	var today = new Date();
	
	return today
}
console.log(dateNow())



fetch('https://jsonplaceholder.typicode.com/todos/2', {
	method: 'PUT',
	headers: {'Content-Type': 'application/json'},
	body: JSON.stringify({
		Title: 'Updated post',
		Description: 'Updated Description',
		Status: 'Completed',
		'Date Completed': dateNow(),
		'User ID': 1

	})
})
.then((response) => response.json())
.then((data) => console.log(data))

// 10. Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API

fetch('https://jsonplaceholder.typicode.com/todos/3', {
	method: 'PATCH',
	headers: {'Content-Type': 'application/json'},
	body: JSON.stringify({
		title: 'Updated post'
	})
})
.then((response) => response.json())
.then((data) => console.log(data))

// 11. Update a to do list item by changing the status to complete and add a date when the status was changed.




fetch('https://jsonplaceholder.typicode.com/todos/6', {
	method: 'PUT',
	headers: {'Content-Type': 'application/json'},
	body: JSON.stringify({
		completed: true,
		date: dateNow()
	})
})
.then((response) => response.json())
.then((data) => console.log(data))




// 12. Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API

fetch('https://jsonplaceholder.typicode.com/posts/5', {
	method: 'DELETE'
})
.then((response) => response.json())
.then((data) => console.log(data))


/*
13. Create a request via Postman to retrieve all the to do list items.
a. GET HTTP method
b. https://jsonplaceholder.typicode.com/todos URI endpoint
c. Save this request as get all to do list items
*/


/*
14. Create a request via Postman to retrieve an individual to do list 
item.
a. GET HTTP method
b. https://jsonplaceholder.typicode.com/todos/1 URI endpoint
c. Save this request as get to do list ite
*/

/*
15. Create a request via Postman to create a to do list item.
a. POST HTTP method
b. https://jsonplaceholder.typicode.com/todos URI endpoint
c. Save this request as create to do list item
*/


/*
16. Create a request via Postman to update a to do list item.
a. PUT HTTP method
b. https://jsonplaceholder.typicode.com/todos/1 URI endpoint
c. Save this request as update to do list item PUT
d. Update the to do list item to mirror the data structure used in the PUT fetch 
request
*/

